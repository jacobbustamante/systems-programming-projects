#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include "Report.h"

int main(int argc, char *argv[]) {
   int timeEnd = 0, fixed = 0;
   int fdin1, fdin2, fdout1, fdout2, fdout3;
   Report report, getReport1, getReport2;

   fdin1 = fdin2 = fdout1 = fdout2 = fdout3 = -1;
   report.id = report.step = 0;
   report.value = 0;

   while (--argc) {
      switch (**++argv) {
         case 'S':
            timeEnd = atoi(*argv + 1);
            break;
         case 'O':
            if (fdout1 < 0)
               fdout1 = atoi(*argv + 1);
            else if (fdout2 < 0)
               fdout2 = atoi(*argv + 1);
            else
               fdout3 = atoi(*argv + 1);
            break;
         case 'I': 
            if (fdin1 < 0)
               fdin1 = atoi(*argv + 1);
            else
               fdin2 = atoi(*argv + 1); 
            break;
         case 'V':
            report.value = atoi(*argv + 1);
            fixed = 1;
            break;
         case '#':
            report.id = atoi(*argv + 1);
            break;
         default:
            break;
      }
   }

   while (report.step < timeEnd + 1) {
      if (fdout1 >= 0)
         write(fdout1, &report, sizeof(Report));
      if (fdout2 >= 0)
         write(fdout2, &report, sizeof(Report));
      if (fdout3 >= 0)
         write(fdout3, &report, sizeof(Report));
      
      if (!fixed) {
         if (fdin1 >= 0)
            read(fdin1, &getReport1, sizeof(Report));
         if (fdin2 >= 0)
            read(fdin2, &getReport2, sizeof(Report));

         report.value = (getReport1.value + getReport2.value) / 2; 
      }
      report.step++;
   }

   close(fdout1);
   close(fdout2);
   close(fdout3);
   close(fdin1);
   close(fdin2);

   if (fixed)
      return 42;
   else
      return 0;
}
