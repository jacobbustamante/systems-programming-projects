#include <sys/unistd.h>
#include <stdio.h>

#include "Branch.h"

static int allowed = MAX_FORKS;

int branch() {
   int childPid = -1;
   
   if (allowed-- > 0) {
      if (!(childPid = fork()))
         allowed = 0;
   }
   else
      fprintf(stderr, "Fork limit exceeded.\n");
   
   return childPid;
}
