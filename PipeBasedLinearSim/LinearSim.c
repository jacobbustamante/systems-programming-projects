#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <strings.h>
#include <sys/wait.h>

#include "Branch.h"
#include "Report.h"

#define MAX_ARGS 9
#define MAX_STRING 20
#define BASE10 10
#define CHILD_NAME "./Cell"

void ReadResults(int fd);
void WaitCells(int cellCount);

int main() {
   double leftValue, rightValue;
   int endTime, cellNumber, childPID, cellCounter, argCounter = 0;
   char **args, **tempArgs;
   int pipeFDs[2], pipeFDs2[2], pipeFD3[2], *localPipes, toClose[2];
   Report report;

   args = (char**)calloc(sizeof(char*), MAX_ARGS + 1);
   for (tempArgs = args; argCounter < MAX_ARGS; argCounter++)
      *tempArgs++ = (char*)calloc(sizeof(char), MAX_STRING + 1);

   scanf("%lf%lf%d%d", &leftValue, &rightValue, &endTime, &cellNumber);
   localPipes = calloc(sizeof(int), cellNumber);
   pipe(pipeFD3);
   *args = CHILD_NAME;

   for (cellCounter = 0; cellCounter < cellNumber; cellCounter++) {
      argCounter = 1;

      if (!cellCounter)
         snprintf(args[argCounter++], MAX_STRING, "V%lf", leftValue);
      else if (cellCounter == cellNumber - 1)
         snprintf(args[argCounter++], MAX_STRING, "V%lf", rightValue);

      if (cellCounter > 1) {
         snprintf(args[argCounter++], MAX_STRING, "O%d", pipeFDs2[1]);
         toClose[1] = pipeFDs2[1];
      }
      if (cellCounter && cellCounter < cellNumber - 1) {
         snprintf(args[argCounter++], MAX_STRING, "I%d", pipeFDs[0]);
         toClose[0] = pipeFDs[0];
      }
      if (cellCounter < cellNumber - 2) {
         pipeFDs[0] = pipeFDs[1] = 0;
         pipe(pipeFDs);
         snprintf(args[argCounter++], MAX_STRING, "O%d", pipeFDs[1]);
      }
      if (cellCounter && cellCounter < cellNumber - 1) {
         pipeFDs2[0] = pipeFDs2[1] = 0;
         pipe(pipeFDs2);
         snprintf(args[argCounter++], MAX_STRING, "I%d", pipeFDs2[0]);
      }

      snprintf(args[argCounter++], MAX_STRING, "S%d", endTime);
      snprintf(args[argCounter++], MAX_STRING, "#%d", cellCounter);
      snprintf(args[argCounter++], MAX_STRING, "O%d", pipeFD3[1]);

      *args[argCounter] = '\0';

      if ((childPID = branch()) > 0) {
         if (cellCounter > 1)
            close(toClose[1]);
         if (cellCounter < cellNumber - 2)
            close(pipeFDs[1]);
         if (cellCounter && cellCounter < cellNumber - 1) {
            close(toClose[0]);
            close(pipeFDs2[0]);
         }
      }
      else if (!childPID){
         if (cellCounter < cellNumber - 2)
            close(pipeFDs[0]);
         if (cellCounter && cellCounter < cellNumber - 1)
            close(pipeFDs2[1]);

         close(pipeFD3[0]);
         execvp(*args, args);
      }
   }
   close(pipeFD3[1]);

   ReadResults(pipeFD3[0]);
   WaitCells(cellNumber);
}

void ReadResults(int fd) {
   Report report;
   
   while (read(fd, &report, sizeof(Report))) {
      printf("Result from %d, step %d: %.3lf\n",
       report.id, report.step, report.value);
   }
}

void WaitCells(int cellCount) {
   int status, child, last = 0, max = cellCount - 1, cellNumber = 1;

   while (cellCount--) {
      wait3(&status, 0, NULL);
      if (WEXITSTATUS(status) == 42) {
         if (last)
            child = max;
         else {
            child = 0;
            last = 1;
         }
      }
      else
         child = cellNumber++;
      printf("Child %d exits with %d\n", child, WEXITSTATUS(status));
   }
}
