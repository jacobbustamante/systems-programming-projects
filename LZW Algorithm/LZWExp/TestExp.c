#include <stdio.h>
#include <stdlib.h>
#include "LZWExp.h"
#include "SmartAlloc.h"
#include "MyLib.h"

void Sink(void* set, char* data, int numBytes)
{
   while(numBytes--) {
      printf("%c", *data++);
   }
}

int main() {
   LZWExp exp;
   DataSink dataSink = Sink;
   void *sink;
   uint bits;
   int errorCode, scanResult;

   LZWExpInit(&exp, dataSink, sink);
   do {
      scanResult = scanf("%x", &bits);
      if (scanResult == 0) {
         errorCode = BAD_CODE;
      }
      else if (scanResult != EOF) {
         errorCode = LZWExpDecode(&exp, bits);
      }
   }
   while (scanResult != EOF && !errorCode);
   if (errorCode == BAD_CODE) {
      printf("Bad code\n");
   }
   else if (LZWExpStop(&exp) == NO_EOD_CODE) {
      printf("Missing EOD\n");
   }
   LZWExpDestruct(&exp);
   if (report_space()) {
      printf("Allocated space: %d\n", report_space());
   }
}
