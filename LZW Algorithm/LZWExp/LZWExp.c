#include <stdlib.h>
#include <stdio.h>
#include "LZWExp.h"
#include "CodeSet.h"
#include "SmartAlloc.h"

#define INT_SIZE BITS_PER_BYTE * sizeof(int)
#define RECYCLE_CODE 4096
#define DICT_ORIG_SIZE 256
#define ORIG_MAX_CODE 511
#define ORIG_NUM_BITS 9
 
void LZWExpInit(LZWExp *exp, DataSink sink, void *sinkState) {
   uchar character = 0;
   void* dictionary = createCodeSet(RECYCLE_CODE);

   do {
      newCode(dictionary, character);
   }
   while (++character);
   exp->lastCode = newCode(dictionary, 0);
   
   exp->dict = dictionary;
   exp->sink = sink;
   exp->sinkState = sinkState;
   exp->numBits = ORIG_NUM_BITS;
   exp->maxCode = ORIG_MAX_CODE;
   exp->leftover = 0;
   exp->bitsLeft = 0;
   exp->EODSeen = 0;   
}

int LZWExpDecode(LZWExp *exp, uint bits) {
   int curBit = 0, errorCode = 0;
   uint code;
   uchar dictResetChar = 0;
 
   while (curBit + exp->numBits <= INT_SIZE) {
      curBit += exp->numBits - exp->bitsLeft;
      code = exp->leftover << exp->numBits - exp->bitsLeft &
      (1 << exp->numBits) - 1 |
      bits >> INT_SIZE - curBit & (1 << exp->numBits - exp->bitsLeft) - 1;
      exp->leftover = exp->bitsLeft = 0;
      if(code > exp->lastCode + 1 || code && exp->EODSeen) {
         errorCode = BAD_CODE;
         return errorCode;
      }
      if (!exp->EODSeen && !(exp->EODSeen = code == DICT_ORIG_SIZE)) {
         if (exp->lastCode + 1 == RECYCLE_CODE) {
            exp->sink(exp->sinkState, getCode(exp->dict, code).data,
            getCode(exp->dict, code).size);
            destroyCodeSet(exp->dict);
            exp->dict = createCodeSet(RECYCLE_CODE);
            do {
               newCode(exp->dict, dictResetChar);
            }
            while (++dictResetChar);         
            exp->lastCode = newCode(exp->dict, 0);
            exp->numBits = ORIG_NUM_BITS;
            exp->maxCode = ORIG_MAX_CODE;
         }
         else {
            if (exp->lastCode != DICT_ORIG_SIZE) {
               setSuffix(exp->dict, exp->lastCode,
               *getCode(exp->dict, code).data);
            }
            exp->sink(exp->sinkState, getCode(exp->dict, code).data,
            getCode(exp->dict, code).size);
            exp->lastCode = extendCode(exp->dict, code);
         }
         if (exp->lastCode > exp->maxCode) {
            exp->maxCode = exp->maxCode * 2 + 1;
            exp->numBits++;
         }
      }
   }
   exp->bitsLeft = INT_SIZE - curBit;
   exp->leftover = bits;
   if (exp->EODSeen && exp->bitsLeft && bits << curBit)
      errorCode = BAD_CODE;
   return errorCode;
}

int LZWExpStop(LZWExp *exp) {
   int error = 0;
   exp->sink(exp->sinkState, NULL, 0);
   if (!exp->EODSeen) {
      error = NO_EOD_CODE;
   }
   return error;
}

void LZWExpDestruct(LZWExp *exp) {
   destroyCodeSet(exp->dict);
}
