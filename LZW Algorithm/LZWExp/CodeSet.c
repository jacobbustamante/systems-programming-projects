#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "SmartAlloc.h"
#include "CodeSet.h"

#define BLOCK_SIZE 4096

typedef struct MemBlock {
   char data[BLOCK_SIZE];
   int bytesUsed;
   struct MemBlock *next;
} MemBlock;

typedef struct CodeSet
{
  Code* codeAr;
  MemBlock* memBlock;
  int size;
  int index;
} CodeSet;

void *createCodeSet(int numCodes)
{
  CodeSet* codeSet = (CodeSet*)malloc(sizeof(CodeSet));
  codeSet->codeAr = (Code*)malloc(sizeof(Code) * numCodes);
  codeSet->size = numCodes;
  codeSet->index = 0;
  codeSet->memBlock = NULL;
  return (void*)codeSet;
}

int newCode(void *codeSet, char val)
{
   CodeSet* cs = (CodeSet*)codeSet;
   int index = (cs->index)++;
   MemBlock* mem;
  
   (((CodeSet*)codeSet)->size)++;
   if(!index)
   {
      mem = (MemBlock*)malloc(sizeof(MemBlock));
      mem->bytesUsed = 0;
      mem->next = NULL;
      cs->memBlock = mem;
   }
   else
   {
      mem = cs->memBlock;
      while(mem->next)
      {
         mem = mem->next;
      }
      if(mem->bytesUsed == BLOCK_SIZE)
      {
         mem->next = (MemBlock*)malloc(sizeof(MemBlock));
         mem = mem->next;
         mem->bytesUsed = 0;
         mem->next = NULL;
      }
   }
   Code* code = cs->codeAr;
   (code+index)->size = 1;
   (code+index)->data = (char*)((mem->data) + mem->bytesUsed);
   (mem->data)[mem->bytesUsed++] = val;
   return index;
}

int extendCode(void *codeSet, int oldCode)
{
   CodeSet* cs = (CodeSet*)codeSet;
   int index = (cs->index)++;
   Code *code, copy;
   MemBlock* mem;
    
   (cs->size)++;
   code = (cs->codeAr) + index;
   copy = getCode(codeSet, oldCode);
   mem = cs->memBlock;
    
   while(mem->next)
   {
     mem = mem->next;
   }
   if((BLOCK_SIZE - mem->bytesUsed) < (copy.size + 1))
   {
     mem->next = (MemBlock*)malloc(sizeof(MemBlock));
     mem = mem->next;
     mem->bytesUsed = 0;
     mem->next = NULL;
   }
   memcpy(&(mem->data[mem->bytesUsed]), copy.data, copy.size);
   mem->data[mem->bytesUsed + copy.size] = 0;
   mem->bytesUsed += copy.size + 1;
   code->data = (char*)(&mem->data[mem->bytesUsed - copy.size - 1]);
   code->size = copy.size + 1;
   return index;
}

void setSuffix(void *codeSet, int code, char suffix)
{
  Code tempCode = getCode(codeSet, code);
  
  tempCode.data[tempCode.size - 1] = suffix;
}

Code getCode(void *codeSet, int code)
{
  return ((CodeSet*)codeSet)->codeAr[code];
}

void destroyCodeSet(void *codeSet)
{
  CodeSet cs = *(CodeSet*)codeSet;
  Code* code = cs.codeAr;
  MemBlock* mem = cs.memBlock;
  MemBlock* temp;
  
  while(mem)
  {
    temp = mem->next;
    free(mem);
    mem = temp;
  }
  free(code);
  free(codeSet);
}
