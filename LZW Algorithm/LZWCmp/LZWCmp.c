#include <stdio.h>
#include "LZWCmp.h"
#include "SmartAlloc.h"

#define ORIG_NUM_BITS 9
#define ORIG_MAX_BITS 511
#define NO_SYM NUM_SYMS
#define INT_SIZE BITS_PER_BYTE * sizeof(int)
#define RECYCLE_CODE 4096

static TrieNode *freeList;

static TrieNode* TrieNodeInit(TrieNode **head) {
   int symbol = 0;

   if (freeList) {
      *head = freeList;
      freeList = freeList->links[0];
   }
   else
      *head = calloc(1, sizeof(TrieNode));

   while (symbol < NUM_SYMS) {
      (*head)->codes[symbol] = symbol;
      (*head)->links[symbol++] = NULL;
   }

   return *head;
}

static void FreeNode(TrieNode *node) {
   int index;

   for (index = 0; index < NUM_SYMS; index++) {
      if (node->links[index])
         FreeNode(node->links[index]);
   }

   if (freeList)
      node->links[0] = freeList;
   freeList = node;
}

static void BuildCode(LZWCmp *cmp, uint code) {
   code <<= INT_SIZE - cmp->numBits;
   cmp->nextInt |= code >> cmp->bitsUsed;
   cmp->bitsUsed += cmp->numBits;

   if (cmp->bitsUsed >= INT_SIZE) {
      cmp->sink(cmp->sinkState, cmp->nextInt);
      cmp->bitsUsed -= INT_SIZE;
      cmp->nextInt = code << cmp->numBits - cmp->bitsUsed;
   }
}

void LZWCmpInit(LZWCmp *cmp, CodeSink sink, void *sinkState) {
   cmp->head = TrieNodeInit(&cmp->head);
   cmp->curLoc = cmp->head;

   cmp->sink = sink;
   cmp->sinkState = sinkState;

   cmp->nextCode = NUM_SYMS + 1;
   cmp->numBits = ORIG_NUM_BITS;
   cmp->maxCode = ORIG_MAX_BITS;

   cmp->nextInt = 0;
   cmp->bitsUsed = 0;
   cmp->lastSym = NO_SYM;
}

void LZWCmpEncode(LZWCmp *cmp, uchar sym) {
   if (cmp->lastSym != NO_SYM) {
      if (cmp->curLoc->links[cmp->lastSym] &&
       cmp->curLoc->links[cmp->lastSym]->codes[sym] != sym) {
         cmp->curLoc = cmp->curLoc->links[cmp->lastSym];
      }
      else {
         if (!cmp->curLoc->links[cmp->lastSym])
            TrieNodeInit(&cmp->curLoc->links[cmp->lastSym]);

         BuildCode(cmp, cmp->curLoc->codes[cmp->lastSym]);

         if (cmp->nextCode == RECYCLE_CODE) {
            FreeNode(cmp->head);
            cmp->head = TrieNodeInit(&cmp->head);

            cmp->numBits = ORIG_NUM_BITS;
            cmp->maxCode = ORIG_MAX_BITS;
            cmp->nextCode = NUM_SYMS + 1;
         }
         else
            cmp->curLoc->links[cmp->lastSym]->codes[sym] = cmp->nextCode++;

         cmp->curLoc = cmp->head;
 
         if (cmp->nextCode - 1 > cmp->maxCode) {
            cmp->numBits++;
            cmp->maxCode = (cmp->maxCode << 1) + 1;
         }
      }
   }
   cmp->lastSym = sym;
}

void LZWCmpStop(LZWCmp *cmp) {
   if (cmp->lastSym != NO_SYM)
      BuildCode(cmp, cmp->curLoc->codes[cmp->lastSym]);

   BuildCode(cmp, NUM_SYMS);

   if (cmp->bitsUsed)
      cmp->sink(cmp->sinkState, cmp->nextInt);
}

void LZWCmpDestruct(LZWCmp *cmp) {
   FreeNode(cmp->head);
}

void LZWCmpClearFreelist() {
   TrieNode *temp;

   while (freeList) {
      temp = freeList;
      freeList = freeList->links[0];
      free(temp);
   }
}
