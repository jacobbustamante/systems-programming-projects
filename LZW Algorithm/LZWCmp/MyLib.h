#ifndef MYLIB_H
#define MYLIB_H

#define BITS_PER_BYTE 8

typedef unsigned char uchar;
typedef unsigned long ulong;
typedef unsigned int uint;
typedef unsigned short ushort;

#ifdef LITTLE_ENDIAN

#define UShortEndianXfer(val) ((val) >> 8 | (val) << 8}

#else

#define UShortEndianXfer(val) (val)

#endif

#endif

