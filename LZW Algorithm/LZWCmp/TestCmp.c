#include <stdio.h>
#include <stdlib.h>
#include "LZWCmp.h"
#include "SmartAlloc.h"

#define LINE_BREAK 7

void Sink(void *format, uint code);

int main() {
   int formatCount = 0;
   uchar symbol;
   LZWCmp cmp;
   CodeSink codeSink = Sink;

   LZWCmpInit(&cmp, codeSink, &formatCount);
   while (scanf("%c", &symbol) != EOF)
      LZWCmpEncode(&cmp, symbol);


   LZWCmpStop(&cmp);
   printf("\n");

   LZWCmpDestruct(&cmp);
   LZWCmpClearFreelist();

   if (report_space() != 0) {
      printf("Allocated space after LZWCmpClearFreelist: %ld\n",
       report_space());
   }
}

void Sink(void *format, uint code) {
   if (*(int*)format < LINE_BREAK) {
      printf("%08X ", code);
      (*(int*)format)++;
   }
   else {
      printf("%08X\n", code);
      *(int*)format = 0;
   }
}
