#include <stdlib.h>

#include "Cell.h"
#include "Report.h"
#include "ReportQueue.h"

void *CellMain(void *cellCfg) {
   int ioCounter;
   double avg;
   CellConfig cellConfig = *(CellConfig*)cellCfg;
   Report report = {0, 0, 0.0}, getReport;

   report.id = cellConfig.id;
   if (!cellConfig.numIns)
      report.value = cellConfig.fixedVal;

   while (report.step < cellConfig.endTime + 1) {
      for (ioCounter = 0; ioCounter < cellConfig.numOuts; ioCounter++)
         RQAdd(cellConfig.outputs[ioCounter], report);

      if (cellConfig.numIns) {
         avg = 0;
         for (ioCounter = 0; ioCounter < cellConfig.numIns; ioCounter++) {
            getReport = RQRemove(cellConfig.inputs[ioCounter]);
            avg += getReport.value;
         }
         report.value = avg / cellConfig.numIns;
      }
      report.step++;
   }
   return NULL;
}
