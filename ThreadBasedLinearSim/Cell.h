#ifndef CELL_H
#define CELL_H

#include "ReportQueue.h"

// Describes one Cell's configuration, including input and
// output queues, ending time of simulation, id for use in
// reporting, and a possible fixed value.
typedef struct {
   int id;                // ID number for reports
   int endTime;           // Simulate up to this time, inclusive
   double fixedVal;       // Use this as fixed value if you numIns == 0
   int numIns;            // Input queue count
   ReportQueue **inputs;  // Input queues
   int numOuts;           // Output queue count
   ReportQueue **outputs; // Output queues
} CellConfig;

// Run this as a thread main function, with a CellConfig passed in,
// and NULL returned.  Do not assume ownership of the CellConfig.
void *CellMain(void *cellCfg);

#endif
