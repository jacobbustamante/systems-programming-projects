#ifndef REPORTQUEUE_H
#define REPORTQUEUE_H

#include "Report.h"

typedef struct ReportQueue ReportQueue;

// Create and return a fully-reentrant, empty, ReportQueue
ReportQueue *RQCreate();

// Destroy a ReportQueue, freeing all associated storage
void RQDestroy(ReportQueue *);

// Add a Report to the end of a ReportQueue, reentrantly. 
// An arbitrary number of threads may call RQAdd simultaenously,
// and appopriate use of mutex/semaphore will ensure no race conditions.
void RQAdd(ReportQueue *, Report);

// Remove and return one Report from a ReportQueue, reentrantly.  And,
// importantly, if the ReportQueue is empty, block until some Report
// becomes available to return, *without spin waiting*.
Report RQRemove(ReportQueue *);

#endif
