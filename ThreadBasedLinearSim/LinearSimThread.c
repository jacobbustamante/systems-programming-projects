#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#include "Cell.h"
#include "Report.h"
#include "ReportQueue.h"

#define NUM_RQ 3
#define MAX_IO 3

void ReadResults(ReportQueue *reportQueue, int cellNumber, int endTime);
void JoinThreads(pthread_t *threads, int cellNumber);
void FreeReports(ReportQueue **reportQueue, int reportCount);
void FreeConfigs(CellConfig **config, int cellNumber);

int main() {
   double leftValue, rightValue;
   int endTime, cellNumber, cell, reportCount = 0;
   CellConfig **config;
   Report report;
   ReportQueue **reportQueue, **toRemove;
   pthread_t *threads;

   scanf("%lf%lf%d%d", &leftValue, &rightValue, &endTime, &cellNumber);
   reportQueue = calloc(sizeof(ReportQueue*), NUM_RQ);
   reportQueue[2] = RQCreate();
   config = calloc(sizeof(CellConfig*), cellNumber);
   threads = calloc(sizeof(pthread_t), cellNumber);

   if (cellNumber >= 0)
      toRemove = calloc(sizeof(ReportQueue*), cellNumber * 2);

   for (cell = 0; cell < cellNumber; cell++) {
      config[cell] = calloc(sizeof(CellConfig), 1);
      config[cell]->id = cell;
      config[cell]->endTime = endTime;
      if (!cell)
         config[cell]->fixedVal = leftValue;
      else if (cell == cellNumber - 1)
         config[cell]->fixedVal = rightValue;
      else
         config[cell]->fixedVal = 0.0;

      config[cell]->numIns = config[cell]->numOuts = 0;
      config[cell]->inputs = calloc(sizeof(ReportQueue*), MAX_IO + 1);
      config[cell]->outputs = calloc(sizeof(ReportQueue*), MAX_IO + 1);

      if (cell > 1)
         config[cell]->outputs[config[cell]->numOuts++] = reportQueue[1];
      if (cell && cell < cellNumber - 1)
         config[cell]->inputs[config[cell]->numIns++] = reportQueue[0];
      if (cell < cellNumber - 2) {
         reportQueue[0] = toRemove[reportCount++] = RQCreate();
         config[cell]->outputs[config[cell]->numOuts++] = reportQueue[0];
      }
      if (cell && cell < cellNumber - 1) {
         reportQueue[1] = toRemove[reportCount++] = RQCreate();
         config[cell]->inputs[config[cell]->numIns++] = reportQueue[1];
      }
      config[cell]->outputs[config[cell]->numOuts++] = reportQueue[2];

      pthread_create(threads + cell, NULL, CellMain, config[cell]);
   }
   ReadResults(reportQueue[2], cellNumber, endTime);
   JoinThreads(threads, cellNumber);
   FreeReports(toRemove, reportCount);
   FreeConfigs(config, cellNumber);
   free(reportQueue);
}

void ReadResults(ReportQueue *reportQueue, int cellNumber, int endTime) {
   int resultCounter, endCount = cellNumber * (endTime + 1);
   Report report;
   
   for (resultCounter = 0; resultCounter < endCount; resultCounter++) {
      report = RQRemove(reportQueue);
      printf("Result from %d, step %d: %.3lf\n",
       report.id, report.step, report.value);
   }
   RQDestroy(reportQueue);
}

void JoinThreads(pthread_t *threads, int cellNumber) {
   int thread;

   for (thread = 0; thread < cellNumber; thread++) {
      pthread_join(threads[thread], NULL);
      printf("Cell thread %d joined\n", thread);
   }
   free(threads);
}

void FreeReports(ReportQueue **reportQueue, int reportCount) {
   int report;

   for (report = 0; report < reportCount; report++)
      RQDestroy(reportQueue[report]);
   free(reportQueue);
}

void FreeConfigs(CellConfig **config, int cellNumber) {
   int cell;

   for (cell = 0; cell < cellNumber; cell++) {
      free(config[cell]->inputs);
      free(config[cell]->outputs);
      free(config[cell]);
   }
   free(config);
}
