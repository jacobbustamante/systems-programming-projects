/*
 * ReportQueue.c
 * Threadsafe functions to keep track of ReportQueues.
 *
 * Author: Jacob Bustamante
 *
 * Version History:
 *    3/18/13 - v1.0  Unix Version
 *    3/18/13 - v1.0m MacOSX Version
 *
 * Changes from Unix version: 
 * 1. #include <stdio.h> and <unistd.h>
 * 2. In RQCreate: Use of sem_open() instead of calloc and sem_init()
 *                 Implemented static variable to uniquely name semaphores
 * 3. In RQDestroy: Use of sem_close() instead of sem_destroy()
 *
 * Notes:
 * Built for Mac OSX. Mac OS doesn't have the same semaphore
 * implementation as the unix implementation. Mac OSX does not support
 * anonymous semphore creation or destroy (sem_init(), sem_destroy()).
 * This means UNIQUELY named semaphores are required.
 */

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/types.h>
#include <unistd.h>

#include "ReportQueue.h"

#define MAX_NAME 20

struct ReportQueue{
   Report report;
   int size;
   pthread_mutex_t *mutex;
   sem_t *removeLock;
   ReportQueue *next;
   ReportQueue *prev;
};

static int reportQueueCounter = 0;

ReportQueue* RQCreate() {
   ReportQueue *reportQueue;
   char nameCounter[MAX_NAME];

   reportQueue = calloc(sizeof(ReportQueue), 1);
   reportQueue->next = reportQueue->prev = reportQueue;
   reportQueue->size = 0;

   reportQueue->mutex = calloc(sizeof(pthread_mutex_t), 1);
   pthread_mutex_init(reportQueue->mutex, NULL);

   snprintf(nameCounter, MAX_NAME, "%d", reportQueueCounter++);
   reportQueue->removeLock = sem_open(nameCounter, O_CREAT, 0644, 0);
   //reportQueue->removeLock = calloc(sizeof(sem_t), 1);
   //sem_init(reportQueue->removeLock, 0, 0);

   return reportQueue;
}

void RQDestroy(ReportQueue *reportQueue) {
   int size = reportQueue->size;
   ReportQueue *tempReportQueue = reportQueue;

   pthread_mutex_lock(reportQueue->mutex);
   while (size-- > 1) {
      tempReportQueue = reportQueue;
      reportQueue = reportQueue->next;
      free(tempReportQueue);
   }
   sem_close(reportQueue->removeLock);
   //sem_destroy(reportQueue->removeLock);
   pthread_mutex_unlock(reportQueue->mutex);
   pthread_mutex_destroy(reportQueue->mutex);

   free(reportQueue->mutex);
   //free(reportQueue->removeLock);
   free(reportQueue);
}

void RQAdd(ReportQueue *reportQueue, Report report) {
   ReportQueue *toAdd;

   pthread_mutex_lock(reportQueue->mutex);

   if (reportQueue->size) {
      toAdd = calloc(sizeof(ReportQueue), 1);
      toAdd->report = report;
      toAdd->size = ++reportQueue->size;
      toAdd->prev = reportQueue->prev;
      toAdd->next = reportQueue;

      toAdd->mutex = reportQueue->mutex;
      toAdd->removeLock = reportQueue->removeLock;

      reportQueue->prev->next = toAdd;
      reportQueue->prev = toAdd;
   }
   else {
      reportQueue->report = report;
      reportQueue->size++;
   }
   sem_post(reportQueue->removeLock);
   pthread_mutex_unlock(reportQueue->mutex);
}

Report RQRemove(ReportQueue *reportQueue) {
   Report report;
   ReportQueue *tempReportQueue;

   sem_wait(reportQueue->removeLock);
   pthread_mutex_lock(reportQueue->mutex);

   tempReportQueue = reportQueue->next;
   report = reportQueue->report;
   reportQueue->size--;
   reportQueue->report = reportQueue->next->report;
   reportQueue->next = reportQueue->next->next;
   reportQueue->next->prev = reportQueue;

   if (reportQueue->size)
      free(tempReportQueue);
   pthread_mutex_unlock(reportQueue->mutex);

   return report;
}