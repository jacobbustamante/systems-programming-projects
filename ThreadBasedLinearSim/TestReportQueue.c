#include <stdio.h>

#include "ReportQueue.h"
#include "Report.h"
#include "SmartAlloc.h" // TODO: Remove from ReportQueue.h before submitting

#define CELL_COUNT 5

void TestRQCreate();
void TestRQAdd();
void TestRQDestroy();
Report ReportCreate(int, int, double);
int ReportEquals(Report report1, Report report2);
void PrintError(char*, long, long);

int main() {
   TestRQCreate();
   TestRQAdd();
   TestRQDestroy();

   return 0;
}

void TestRQCreate() {
   ReportQueue *reportQueue;

   reportQueue = RQCreate();
   if(!reportQueue) {
      PrintError("TestRQCreate: reportQueue ", 1, (long)reportQueue);
      return;
   }

   if(!report_space()) {
      PrintError("TestRQCreate: space ", 160, report_space());
      return;
   }

   RQDestroy(reportQueue);
}

void TestRQAdd() {
   ReportQueue *reportQueue;
   Report report1, report2;
   int status;

   reportQueue = RQCreate();

   report1 = ReportCreate(0, 0, 4.0);
   RQAdd(reportQueue, report1);
   report2 = RQRemove(reportQueue);
   if (status = ReportEquals(report1, report2))
      PrintError("TestRQAdd: (one) ", 0, status);

   RQDestroy(reportQueue);
   reportQueue = RQCreate();

   RQAdd(reportQueue, ReportCreate(0, 1, 5.0));
   RQAdd(reportQueue, ReportCreate(0, 2, 6.0));
   RQAdd(reportQueue, ReportCreate(0, 3, 7.0));

   report1 = ReportCreate(0, 1, 5.0);
   report2 = RQRemove(reportQueue);
   if (status = ReportEquals(report1, report2)) {
      PrintError("TestRQAdd: (multiple) ", 0, status);
      return;
   }

   report1 = ReportCreate(0, 2, 6.0);
   report2 = RQRemove(reportQueue);
   if (status = ReportEquals(report1, report2)) {
      PrintError("TestRQAdd: (multiple) ", 0, status);
      return;
   }

   report1 = ReportCreate(0, 3, 7.0);
   report2 = RQRemove(reportQueue);
   if (status = ReportEquals(report1, report2)) {
      PrintError("TestRQAdd: (multiple) ", 0, status);
      return;
   }

   RQDestroy(reportQueue);
}

void TestRQDestroy() {
   ReportQueue *reportQueue;
   
   reportQueue = RQCreate();
   RQDestroy(reportQueue);
   if (report_space())
      PrintError("TestRQDestroy: (none) ", 0, report_space());

   reportQueue = RQCreate();
   RQAdd(reportQueue, ReportCreate(4, 8, 15.16));
   RQDestroy(reportQueue);
   if (report_space())
      PrintError("TestRQDestroy: (one-add) ", 0, report_space());

   reportQueue = RQCreate();
   RQAdd(reportQueue, ReportCreate(4, 8, 15.16));
   RQRemove(reportQueue);
   RQDestroy(reportQueue);
   if (report_space())
      PrintError("TestRQDestroy: (one-remove) ", 0, report_space());

   reportQueue = RQCreate();
   RQAdd(reportQueue, ReportCreate(0, 0, 4.0));
   RQAdd(reportQueue, ReportCreate(3, 1, 8.1));
   RQAdd(reportQueue, ReportCreate(3, 3, 15.2));
   RQAdd(reportQueue, ReportCreate(5, 4, 16.3));
   RQAdd(reportQueue, ReportCreate(9, 6, 23.4));
   RQAdd(reportQueue, ReportCreate(6, 3, 42.5));

   RQDestroy(reportQueue);
   if (report_space())
      PrintError("TestRQDestroy: (multiple) ", 0, report_space());
}

Report ReportCreate(int id, int step, double value) {
   Report report = {id, step, value};

   return report;
}

int ReportEquals(Report report1, Report report2) {
   if (report1.id == report2.id)
      if (report1.step == report2.step)
         if (report1.value == report2.value)
            return 0;
   return 1;
}

void PrintError(char* test, long expected, long returned) {
   printf("Error %s expected %ld, returned %ld.\n", test, expected, returned);
}
