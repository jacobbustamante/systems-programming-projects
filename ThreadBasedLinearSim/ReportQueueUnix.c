#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/types.h>

#include "ReportQueue.h"

struct ReportQueue{
   Report report;
   int size;
   pthread_mutex_t *mutex;
   sem_t *removeLock;
   ReportQueue *next;
   ReportQueue *prev;
};

ReportQueue* RQCreate() {
   ReportQueue *reportQueue;

   reportQueue = calloc(sizeof(ReportQueue), 1);
   reportQueue->next = reportQueue->prev = reportQueue;
   reportQueue->size = 0;

   reportQueue->mutex = calloc(sizeof(pthread_mutex_t), 1);
   pthread_mutex_init(reportQueue->mutex, NULL);
   reportQueue->removeLock = calloc(sizeof(sem_t), 1);
   sem_init(reportQueue->removeLock, 0, 0);

   return reportQueue;
}

void RQDestroy(ReportQueue *reportQueue) {
   int size = reportQueue->size;
   ReportQueue *tempReportQueue = reportQueue;

   pthread_mutex_lock(reportQueue->mutex);
   while (size-- > 1) {
      tempReportQueue = reportQueue;
      reportQueue = reportQueue->next;
      free(tempReportQueue);
   }
   sem_destroy(reportQueue->removeLock);
   pthread_mutex_unlock(reportQueue->mutex);
   pthread_mutex_destroy(reportQueue->mutex);

   free(reportQueue->mutex);
   free(reportQueue->removeLock);
   free(reportQueue);
}

void RQAdd(ReportQueue *reportQueue, Report report) {
   ReportQueue *toAdd;

   pthread_mutex_lock(reportQueue->mutex);

   if (reportQueue->size) {
      toAdd = calloc(sizeof(ReportQueue), 1);
      toAdd->report = report;
      toAdd->size = ++reportQueue->size;
      toAdd->prev = reportQueue->prev;
      toAdd->next = reportQueue;

      toAdd->mutex = reportQueue->mutex;
      toAdd->removeLock = reportQueue->removeLock;

      reportQueue->prev->next = toAdd;
      reportQueue->prev = toAdd;
   }
   else {
      reportQueue->report = report;
      reportQueue->size++;
   }
   sem_post(reportQueue->removeLock);
   pthread_mutex_unlock(reportQueue->mutex);
}

Report RQRemove(ReportQueue *reportQueue) {
   Report report;
   ReportQueue *tempReportQueue;

   sem_wait(reportQueue->removeLock);
   pthread_mutex_lock(reportQueue->mutex);

   tempReportQueue = reportQueue->next;
   report = reportQueue->report;
   reportQueue->size--;
   reportQueue->report = reportQueue->next->report;
   reportQueue->next = reportQueue->next->next;
   reportQueue->next->prev = reportQueue;

   if (reportQueue->size)
      free(tempReportQueue);
   pthread_mutex_unlock(reportQueue->mutex);

   return report;
}
